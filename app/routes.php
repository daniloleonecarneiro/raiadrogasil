<?php
declare(strict_types=1);

use App\Application\Actions\User\ListUsersAction;
use App\Application\Actions\User\ViewUserAction;
use App\Application\Actions\Quote\ViewQuoteAction;
use App\Application\Actions\Quote\CreateQuoteAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Raia Drogasil API!');
        return $response;
    });

    $app->group('/raiadrogasil', function ($app) {
    	$app->group('/v1', function ($app) {
    		$app->group('/quote', function (Group $group) {
                $group->get('/{from}/{to}', ViewQuoteAction::class);
                $group->post('/create', CreateQuoteAction::class);
	        });
	    });
	});
};
