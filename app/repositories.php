<?php
declare(strict_types=1);

use App\Model\Quote\QuoteRepository;
use App\Infrastructure\Persistence\Quote\QuoteRepository as PersistenceQuoteRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        QuoteRepository::class => \DI\autowire(PersistenceQuoteRepository::class)
    ]);
};
