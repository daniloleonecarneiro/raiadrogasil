<?php
declare(strict_types=1);

namespace App\Model\Quote;

/**
 * Class Quote
 * @package App\Model\Quote
 */
class Quote
{
    /**
     * @var string
     */
    private string $from;

    /**
     * @var string
     */
    private string $to;

    /**
     * @var float
     */
    private float $price;

    /**
     * Quote constructor.
     * @param string $from
     * @param string $to
     * @param float $price
     */
    public function __construct(string $from, string $to, float $price)
    {
        $this->from = strtoupper($from);
        $this->to = strtoupper($to);
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }
}
