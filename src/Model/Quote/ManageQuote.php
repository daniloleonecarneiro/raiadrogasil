<?php

declare(strict_types=1);

namespace App\Model\Quote;

/**
 * Class ManageQuote
 * @package App\Model\Quote
 */
class ManageQuote
{
    /**
     * @var array
     */
    private static array $quoteFrom;

    /**
     * @param string $from
     * @param string $to
     * @param array $quotes
     * @return array
     */
    public static function findQuoteFromTo(string $from, string $to, array $quotes): array
    {
        $flights = [];

        foreach ($quotes as $q => $qu) {

            if ($qu[0] == $from) {

                if ($qu[1] == $to) {
                    $flights[] = [
                        'flights' => [
                            'airports' => [$from, $to],
                            'price' => +$qu[2]
                        ]
                    ];
                    continue;
                }

                self::$quoteFrom[] = [
                    'from' => $from,
                    'to' => $to,
                    'excluded' => [$q],
                    'next_to' => [
                        [
                            'to' => $qu[1],
                            'price' => $qu[2]
                        ]
                    ],
                    'flights' => [
                        'airports' => [],
                        'price' => 0
                    ]
                ];
            }
        }

        $flight = self::searchFlights($quotes);

        $routePrice = self::quoteLowerPriceFlight(array_merge($flight,$flights));

        if (empty($routePrice)) {
            return [];
        }

        return [
            "route" => implode(",", $routePrice['flights']['airports']),
            "price" => $routePrice['flights']['price']
        ];
    }

    /**
     * @param array $quotes
     * @return array
     */
    private static function searchFlights(array $quotes): array
    {
        foreach (self::$quoteFrom as $q => $qf) {
            self::searchConnection($q, $quotes, $qf);
        }

        $connectionsTo = self::ConnectionsTo();

        $flights = self::addFlightsAndPrice($connectionsTo);

        $flight = self::quoteLowerPriceFlight($flights);

        if (empty($flight)) {
            return [];
        }

        return [
            [
                'flights' =>
                    [
                        'airports' => $flight['flights']['airports'],
                        'price' => $flight['flights']['price']
                    ]
            ]
        ];

    }

    /**
     * @param int $i
     * @param array $quotes
     */
    private static function searchConnection(int $i, array $quotes): void
    {
        $unverifiedQuote = self::diffQuotes($quotes, self::$quoteFrom[$i]['excluded']);
        $nextTo = end(self::$quoteFrom[$i]['next_to']);
        $_nextTo = [];

        if ($nextTo['to'] != self::$quoteFrom[$i]['to']) {
            foreach ($unverifiedQuote as $u => $uq) {

                $quote = $quotes[$uq];

                if ($nextTo['to'] == $quote[0]) {
                    self::$quoteFrom[$i]['excluded'][] = $uq;
                    $_nextTo = [
                        'to' => $quote[1],
                        'price' => $quote[2]
                    ];

                    break;
                }
            }
        }

        if (!empty($_nextTo)) {
            self::$quoteFrom[$i]['next_to'][] = $_nextTo;
            self::searchConnection($i, $quotes);
        }
    }

    /**
     * @return array
     */
    private static function ConnectionsTo(): array
    {
        $quote = self::$quoteFrom;
        $connectionsTo = [];

        foreach ($quote as $q => $qu) {
            $endNextTo = end($qu["next_to"]);
            $to = $endNextTo['to'];

            if ($to == $qu['to']) {
                $connectionsTo[] = $qu;
            }
        }

        return $connectionsTo;
    }

    /**
     * @param array $connections
     * @return array
     */
    private static function addFlightsAndPrice(array $connections): array
    {
        foreach ($connections as $c => $co) {
            $connections[$c]['flights']['airports'][] = $connections[$c]['from'];
            foreach ($connections[$c]['next_to'] as $n => $nt) {
                $connections[$c]['flights']['airports'][] = $nt['to'];
                $connections[$c]['flights']['price'] += $nt['price'];
            }
        }

        return $connections;
    }

    /**
     * @param array $flights
     * @return array
     */
    private static function quoteLowerPriceFlight(array $flights): array
    {
        $flight = ['key' => 0, 'price' => 0];

        foreach ($flights as $f => $fl) {

            if ($flight['price'] == 0) {
                $flight['price'] = $fl['flights']['price'];
            }

            if ($fl['flights']['price'] < $flight['price']) {
                $flight['key'] = $f;
                $flight['price'] = $fl['flights']['price'];
            }
        }

        if (!array_key_exists($flight['key'], $flights)) {
            return [];
        }

        return $flights[$flight['key']];
    }

    /**
     * @param array $quotes
     * @param array $excluded
     * @return array
     */
    private static function diffQuotes(array $quotes, array $excluded): array
    {
        $quotesKeys = array_keys($quotes);

        return array_diff($quotesKeys, $excluded);
    }
}
