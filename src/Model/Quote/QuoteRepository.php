<?php
declare(strict_types=1);

namespace App\Model\Quote;
use App\Model\Quote\Quote;

/**
 * Interface QuoteRepository
 * @package App\Model\Quote
 */
interface QuoteRepository
{
    /**
     * @return array
     */
    public function findAll(): array;

    /**
     * @return array
     */
    public function create(Quote $quote): void;
}
