<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Quote;

use App\Infrastructure\Persistence\Exception;

class QuoteNotFoundException extends RecordNotFoundException
{
    public $message = 'No airline tickets to were found.';
}