<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Quote;

use App\Model\Quote\Quote;
use App\Model\Quote\QuoteRepository as ModelRepository;
use App\Infrastructure\Persistence\Quote\QuoteNotFoundException;
use Psr\Container\ContainerInterface;

/**
 * Class QuoteRepository
 * @package App\Infrastructure\Persistence\Quote
 */
class QuoteRepository implements ModelRepository
{
    /**
     * @var string
     */
    private string $db;

    /**
     * @var array
     */
    private array $quotes;

    /**
     * QuoteRepository constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->db = $container->get('settings')['db']['path'];
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        $fileHandle = fopen($this->db, "r");

        while (($row = fgetcsv($fileHandle, 0, ",")) !== false) {
            $this->quotes[] = $row;
        }

        return $this->quotes;
    }

    /**
     * @param Quote $quote
     */
    public function create(Quote $quote): void
    {
        $fileHandle = fopen($this->db, "a");

        fputcsv($fileHandle, [
            $quote->getFrom(),
            $quote->getTo(),
            $quote->getPrice()
        ]);

        fclose($fileHandle);
    }
}
