<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Exception;

/**
 * Class RecordNotFoundException
 * @package App\Infrastructure\Persistence\Exception
 */
class RecordNotFoundException extends Exception
{
}
