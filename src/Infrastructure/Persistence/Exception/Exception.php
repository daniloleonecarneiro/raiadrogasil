<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Exception;

use Exception as Ex;

/**
 * Class Exception
 * @package App\Infrastructure\Persistence\Exception
 */
abstract class Exception extends Ex
{
}
