<?php
declare(strict_types=1);

namespace App\Application\Actions\Quote;

use Psr\Http\Message\ResponseInterface as Response;
use App\Model\Quote\Quote;

class CreateQuoteAction extends QuoteAction
{
    private const SUCCESS_MSG = "Quote created success";

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $data = $this->getFormData();

        $quote = new Quote($data->from, $data->to, $data->price);

        $this->quoteRepository->create($quote);

        $this->logger->info(self::SUCCESS_MSG);

        return $this->respondWithData(["result" => self::SUCCESS_MSG]);
    }
}
