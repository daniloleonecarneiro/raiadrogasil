<?php
declare(strict_types=1);

namespace App\Application\Actions\Quote;

use Psr\Http\Message\ResponseInterface as Response;
use App\Model\Quote\ManageQuote;

/**
 * Class ViewQuoteAction
 * @package App\Application\Actions\Quote
 */
class ViewQuoteAction extends QuoteAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $from = $this->resolveArg('from');
        $to = $this->resolveArg('to');

        $quotes = $this->quoteRepository->findAll();

        $quote = ManageQuote::findQuoteFromTo($from, $to, $quotes);

        $this->logger->info("Quote from `${from}` to `${to}` was viewed.");

        return $this->respondWithData($quote);
    }
}
