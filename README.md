# Configuração e Instalação #

### Instalação ###

##### Requisitos #####

### PHP

* PHP: `^7.4`
* Composer

### Estrutura de pastas
```
./
  |_ raiadrogasil //back-end
```

Para o correto funcionamento do projeto, é necessário seguir as instruções configuração


#### drogasil-api ####
	
Para adicionar as dependências da api, acesse a pasta execute o composer:

```
composer install
```

Após a instalção das dependências, acesse a pasta `raiadrogasil` via terminal.

Execute o seguinte comando:

```
composer start
```

Caso tenha `docker-compose` para executar a `docker`, execute o comando:

```
docker-compor up
```